package com.hfad.beeradviser

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get the find_beer button
        val findBeer = findViewById<Button>(R.id.find_beer)
        // Set the click event
        findBeer.setOnClickListener {
            // Get the beer_color spinner's selected item
            val beerColor = findViewById<Spinner>(R.id.beer_color).selectedItem.toString()
            // Get the brands text view
            val brands = findViewById<TextView>(R.id.brands)
            // Set the value to the recommended list of beers
            brands.text = getBeers(beerColor).joinToString(separator="\n") { "$it" }

        }
    }

    // Get a beer color and return list of beer recommendations
    fun getBeers(color:String): List<String> {
        return when (color) {
            "Light" -> listOf("Jail Pale Ale", "Lager Lite")
            "Amber" -> listOf("Jack Amber", "Red Moose")
            "Brown" -> listOf("Brown Bear Beer", "Bock Brownie")
            else -> listOf("Gout Stout", "Dark Daniel")
        }
    }
}