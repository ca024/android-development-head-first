// This is the package name.
package com.hfad.myfirstapp

// These are Android classes used in MainActivity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

// MainActivity extends the class AppCompatActivity
class MainActivity : AppCompatActivity() {
    // Implement the onCreate() method from the AppCompatActivity class.
    override fun onCreate(savedInstanceState: Bundle?) {
        // This method is called when the activity is first created.
        super.onCreate(savedInstanceState)
        // Specify which layout to use.
        setContentView(R.layout.activity_main)
    }
}